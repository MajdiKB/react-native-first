# React-Native My first project.

## Info

This is my first project working with React Native.

If you want to try, you can clone the project in a local folder and after installing the dependencies run the project.

### Clone the project.

`git clone git@gitlab.com:MajdiKB/react-native-first.git`

### Install dependencies.

`npm i`

### Run the project.

1. `npm start`
2. `npm run androind`