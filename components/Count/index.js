import React, {useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import ButtonCustom from '../ButtonCustom/index';

const Count = () => {
  const [state, setState] = useState({
    counter: 0,
  });

  const {counter} = state;

  const handleUp = () => {
    setState({counter: counter + 1});
  };

  const handleDown = () => {
    setState({counter: counter - 1});
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Contador</Text>
      <View style={styles.subcontainer}>
        <ButtonCustom label="-" action={handleDown} />
        <View style={styles.counterContainer}>
          <Text style={styles.counterTxt}>{counter}</Text>
        </View>
        <ButtonCustom label="+" action={handleUp} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2c3e50',
    justifyContent: 'center',
    flex: 1,
  },
  subcontainer: {
    height: 50,
    width: '100%',
    paddingHorizontal: 20,
    flexDirection: 'row',
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: 25,
  },
  counterContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  counterTxt: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Count;
