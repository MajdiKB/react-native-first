import React from 'react';

import {View, StyleSheet, Text} from 'react-native';

const Viewer = () => {
  return (
    <View style={styles.container}>
      <Text>This is another component!</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 45,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Viewer;
