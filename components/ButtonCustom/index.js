import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

const ButtonCustom = (props) => {
  const {label, action} = props;
  return (
    <TouchableOpacity style={styles.btn} onPress={action}>
      <Text style={styles.btnTxt}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ecf0f1',
  },
  btnTxt: {
    fontSize: 25,
    color: 'black',
    justifyContent: 'center',
  },
});

//si no pasamos props, se pasan estas dos props por defecto
ButtonCustom.defaultProps = {
  label: 'button',
  action: () => null,
};

ButtonCustom.propTypes = {
  label: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
};

export default ButtonCustom;
