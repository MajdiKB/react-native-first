/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {StyleSheet, View, Text} from 'react-native';
import Count from './components/Count';
import Viewer from './components/View';

const App = () => {
  return (
    <View style={styles.container}>
      <Viewer />
      <View style={styles.containerRed}>
        <Text>Hello world!</Text>
      </View>
      <View style={styles.container}>
        <Count style={styles.container} />
      </View>
      <View style={styles.container}>
        <Text>Esto es una prueba</Text>
      </View>
      <View style={styles.containerYellow}>
        <Text>Hello Jonay!!</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue',
  },
  containerRed: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  containerYellow: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'yellow',
  },
});

export default App;
